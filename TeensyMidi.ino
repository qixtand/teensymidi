/**
 * This example can be used together with a Teensy 2.0 or later to use the OPL2 Audio Board as a MIDI device. To
 * configure the Teensy as a MIDI device set USB Type to MIDI in the IDE using Tools > USB Type > MIDI. Once connected
 * the board should appear in the device list as 'OPL2 AUdio Board MIDI'. You can now use test the board with, for
 * example, MIDI-OX, your favorite music creation software or DosBox! 
 *
 * OPL2 board is connected as follows:
 *   Pin  8 - Reset
 *   Pin  9 - A0
 *   Pin 10 - Latch
 *   Pin 11 - Data
 *   Pin 13 - Shift
 *
 * Code by Maarten Janssen (maarten@cheerful.nl) 2018-07-02
 * Most recent version of the library can be found at my GitHub: https://github.com/DhrBaksteen/ArduinoOPL2
 */

#include <SPI.h>
#include <OPL2.h>

// For better results with midi files created for Window exchange midi_instruments.h for midi_instruments_win31.h
// #include <midi_instruments_win31.h>
#include <midi_instruments.h>
#include <midi_drums.h>

#define MIDI_NUM_CHANNELS 16
#define MIDI_DRUM_CHANNEL 10

#define CONTROL_0                 0   // Bank Select
#define CONTROL_1                 1   // Modulation Wheel or Lever
#define CONTROL_2                 2   // Breath Controller
#define CONTROL_3                 3   // Undefined
#define CONTROL_4                 4   // Foot Controller
#define CONTROL_5                 5   // Portamento Time
#define CONTROL_6                 6   // Data Entry MSB
#define CONTROL_VOLUME            7   // Channel Volume
#define CONTROL_8                 8   // Balance
#define CONTROL_9                 9   // Undefined
#define CONTROL_10                10  // Pan
#define CONTROL_11                11  // Expression Controller
#define CONTROL_12                12  // Effect Control 1
#define CONTROL_13                13  // Effect Control 2
#define CONTROL_14                14  // Undefined
#define CONTROL_15                15  // Undefined
// CONTROL_GENERAL_PURPOSE_1 selects the instrument
#define CONTROL_GENERAL_PURPOSE_1 16  // General Purpose Controller 1
#define CONTROL_GENERAL_PURPOSE_2 17  // General Purpose Controller 2
#define CONTROL_GENERAL_PURPOSE_3 18  // General Purpose Controller 3
#define CONTROL_GENERAL_PURPOSE_4 19  // General Purpose Controller 4
#define CONTROL_20                20  // Undefined
#define CONTROL_21                21  // Undefined
#define CONTROL_22                22  // Undefined
#define CONTROL_23                23  // Undefined
#define CONTROL_24                24  // Undefined
#define CONTROL_25                25  // Undefined
#define CONTROL_26                26  // Undefined
#define CONTROL_27                27  // Undefined
#define CONTROL_28                28  // Undefined
#define CONTROL_29                29  // Undefined
#define CONTROL_30                30  // Undefined
#define CONTROL_31                31  // Undefined
#define CONTROL_32                32  // LSB for Control 0 (Bank Select)
#define CONTROL_33                33  // LSB for Control 1 (Modulation Wheel or Lever)
#define CONTROL_34                34  // LSB for Control 2 (Breath Controller)
#define CONTROL_35                35  // LSB for Control 3 (Undefined)
#define CONTROL_36                36  // LSB for Control 4 (Foot Controller)
#define CONTROL_37                37  // LSB for Control 5 (Portamento Time)
#define CONTROL_38                38  // LSB for Control 6 (Data Entry)
#define CONTROL_39                39  // LSB for Control 7 (Channel Volume)
#define CONTROL_40                40  // LSB for Control 8 (Balance)
#define CONTROL_41                41  // LSB for Control 9 (Undefined)
#define CONTROL_42                42  // LSB for Control 10 (Pan)
#define CONTROL_43                43  // LSB for Control 11 (Expression Controller)
#define CONTROL_44                44  // LSB for Control 12 (Effect control 1)
#define CONTROL_45                45  // LSB for Control 13 (Effect control 2)
#define CONTROL_46                46  // LSB for Control 14 (Undefined)
#define CONTROL_47                47  // LSB for Control 15 (Undefined)
#define CONTROL_48                48  // LSB for Control 18 (General Purpose Controller 1)
#define CONTROL_49                49  // LSB for Control 18 (General Purpose Controller 2)
#define CONTROL_50                50  // LSB for Control 18 (General Purpose Controller 3) (Beatstep Pro: Record)
#define CONTROL_51                51  // LSB for Control 19 (General Purpose Controller 4) (Beatstep Pro: Stop)
#define CONTROL_52                52  // LSB for Control 20 (Undefined)
#define CONTROL_53                53  // LSB for Control 21 (Undefined)
#define CONTROL_54                54  // LSB for Control 22 (Undefined) (Beatstep Pro: Play)
#define CONTROL_55                55  // LSB for Control 23 (Undefined)
#define CONTROL_56                56  // LSB for Control 24 (Undefined)
#define CONTROL_57                57  // LSB for Control 25 (Undefined)
#define CONTROL_58                58  // LSB for Control 26 (Undefined)
#define CONTROL_59                59  // LSB for Control 27 (Undefined)
#define CONTROL_60                60  // LSB for Control 28 (Undefined)
#define CONTROL_61                61  // LSB for Control 29 (Undefined)
#define CONTROL_62                62  // LSB for Control 30 (Undefined)
#define CONTROL_63                63  // LSB for Control 31 (Undefined)
#define CONTROL_64                64  // Damper Pedal on/off (Sustain) (≤63 off, ≥64 on)
#define CONTROL_65                65  // Portamento On/Off (≤63 off, ≥64 on)
#define CONTROL_66                66  // Sostenuto On/Off (≤63 off, ≥64 on)
#define CONTROL_67                67  // Soft Pedal On/Off (≤63 off, ≥64 on)
#define CONTROL_68                68  // Legato Footswitch (≤63 Normal, ≥64 Legato)
#define CONTROL_69                69  // Hold 2 (≤63 off, ≥64 on)
#define CONTROL_70                70  // Sound Controller 1 (default: Sound Variation)
#define CONTROL_TIMBRE            71  // Sound Controller 2 (default: Timbre/Harmonic Intens.)
#define CONTROL_RELEASE_TIME      72  // Sound Controller 3 (default: Release Time)
#define CONTROL_ATTACK_TIME       73  // Sound Controller 4 (default: Attack Time)
#define CONTROL_BRIGHTNESS        74  // Sound Controller 5 (default: Brightness)
#define CONTROL_DECAY_TIME        75  // Sound Controller 6 (default: Decay Time)
#define CONTROL_76                76  // Sound Controller 7 (default: Vibrato Rate)
#define CONTROL_77                77  // Sound Controller 8 (default: Vibrato Depth)
#define CONTROL_78                78  // Sound Controller 9 (default: Vibrato Delay)
#define CONTROL_79                79  // Sound Controller 10 (default: undefined)
#define CONTROL_GENERAL_PURPOSE_5 80  // General Purpose Controller 5
#define CONTROL_GENERAL_PURPOSE_6 81  // General Purpose Controller 6
#define CONTROL_GENERAL_PURPOSE_7 82  // General Purpose Controller 7
#define CONTROL_GENERAL_PURPOSE_8 83  // General Purpose Controller 8
#define CONTROL_84                84  // Portamento Control
#define CONTROL_85                85  // Undefined
#define CONTROL_86                86  // Undefined
#define CONTROL_87                87  // Undefined
#define CONTROL_88                88  // High Resolution Velocity Prefix
#define CONTROL_89                89  // Undefined
#define CONTROL_90                90  // Undefined
#define CONTROL_91                91  // Effects 1 Depth (default: Reverb Send Level)
#define CONTROL_92                92  // Effects 2 Depth
#define CONTROL_93                93  // Effects 3 Depth (default: Chorus Send Level)
#define CONTROL_94                94  // Effects 4 Depth (formerly Celeste [Detune] Depth)
#define CONTROL_95                95  // Effects 5 Depth (formerly Phaser Depth)
#define CONTROL_96                96  // Data Increment (Data Entry +1)
#define CONTROL_97                97  // Data Decrement (Data Entry -1)
#define CONTROL_98                98  // Non-Registered Parameter Number (NRPN) - LSB
#define CONTROL_99                99  // Non-Registered Parameter Number (NRPN) - MSB
#define CONTROL_100               100 // Registered Parameter Number (RPN) - LSB
#define CONTROL_101               101 // Registered Parameter Number (RPN) - MSB
#define CONTROL_102               102 // Undefined
#define CONTROL_103               103 // Undefined
#define CONTROL_104               104 // Undefined
#define CONTROL_105               105 // Undefined
#define CONTROL_106               106 // Undefined
#define CONTROL_107               107 // Undefined
#define CONTROL_108               108 // Undefined
#define CONTROL_109               109 // Undefined
#define CONTROL_110               110 // Undefined
#define CONTROL_111               111 // Undefined
#define CONTROL_112               112 // Undefined
#define CONTROL_113               113 // Undefined
#define CONTROL_114               114 // Undefined
#define CONTROL_115               115 // Undefined
#define CONTROL_116               116 // Undefined
#define CONTROL_117               117 // Undefined
#define CONTROL_118               118 // Undefined
#define CONTROL_119               119 // Undefined
#define CONTROL_ALL_SOUND_OFF     120 // All Sound Off
#define CONTROL_RESET_ALL         121 // Reset All Controllers
#define CONTROL_LOCAL_CONTROL     122 // Local Control On/Off (0 off, 127 on)
#define CONTROL_ALL_NOTES_OFF     123 // All Notes Off
#define CONTROL_OMNI_MODE_OFF     124 // Omni Mode Off (+ all notes off)
#define CONTROL_OMNI_MODE_ON      125 // Omni Mode Off (+ all notes on)
#define CONTROL_MONO_MODE_ON      126 // Mono Mode On (+ poly off, + all notes off)
#define CONTROL_POLY_MODE_ON      127 // Poly Mode On (+ mono off, +all notes off)

// Channel mapping to keep track of MIDI to OPL2 channel mapping.
struct ChannelMapping {
	byte midiChannel;
	byte midiNote;
	float midiVelocity;
	float op1Level;
	float op2Level;
};


OPL2 opl2;
ChannelMapping channelMap[OPL2_NUM_CHANNELS];
byte oldestChannel[OPL2_NUM_CHANNELS];
byte programMap[MIDI_NUM_CHANNELS];
float channelVolumes[MIDI_NUM_CHANNELS];


/**
 * Register MIDI event handlers and initialize.
 */
void setup() {
	usbMIDI.setHandleNoteOn(onNoteOn);
	usbMIDI.setHandleNoteOff(onNoteOff);
	usbMIDI.setHandleProgramChange(onProgramChange);
	usbMIDI.setHandleControlChange(onControlChange);
	usbMIDI.setHandleSystemReset(onSystemReset);
	onSystemReset();

	opl2.setDeepVibrato(true);
	opl2.setDeepTremolo(true);
}


/**
 * Read and handle MIDI events.
 */
void loop() {
	usbMIDI.read();
}


/**
 * Get a free OPL2 channel to play a note. If all channels are occupied then recycle the oldes one.
 */
byte getFreeChannel(byte midiChannel) {  
	byte opl2Channel = 255;

	// Look for a free OPL2 channel.
	for (byte i = 0; i < OPL2_NUM_CHANNELS; i ++) {
		if (!opl2.getKeyOn(i)) {
			opl2Channel = i;
			break;
		}
	}

	// If no channels are free then recycle the oldest, where drum channels will be the first to be recycled. Only if
	// no drum channels are left will the actual oldest channel be recycled.
	if (opl2Channel == 255) {
		opl2Channel = oldestChannel[0];
		for (byte i = 0; i < OPL2_NUM_CHANNELS; i ++) {
			if (channelMap[oldestChannel[i]].midiChannel == MIDI_DRUM_CHANNEL) {
				opl2Channel = oldestChannel[i];
			}
		}
	}

	// Update the list of last used channels by moving the current channel to the bottom so the last updated channel
	// will move to the front of the list. If no more OPL2 channels are free then the last updated one will be recycled.
	byte i;
	for (i = 0; i < OPL2_NUM_CHANNELS && oldestChannel[i] != opl2Channel; i ++) {}

	while (i < OPL2_NUM_CHANNELS - 1) {
		byte temp = oldestChannel[i + 1];
		oldestChannel[i + 1] = oldestChannel[i];
		oldestChannel[i] = temp;
		i ++;
	}

	return opl2Channel;
}


/**
 * Set the volume of operators 1 and 2 of the given OPL2 channel according to the settings of the given MIDI channel.
 */
void setOpl2ChannelVolume(byte opl2Channel, byte midiChannel) {
	float volume = channelMap[opl2Channel].midiVelocity * channelVolumes[midiChannel];
	byte volumeOp1 = round(channelMap[opl2Channel].op1Level * volume * 63.0);
	byte volumeOp2 = round(channelMap[opl2Channel].op2Level * volume * 63.0);
	opl2.setVolume(opl2Channel, OPERATOR1, 63 - volumeOp1);
	opl2.setVolume(opl2Channel, OPERATOR2, 63 - volumeOp2);
}


/**
 * Handle a note on MIDI event to play a note.
 */
void onNoteOn(byte channel, byte note, byte velocity) {
	channel = channel % 16;

	// Treat notes with a velocity of 0 as note off.
	if (velocity == 0) {
		onNoteOff(channel, note, velocity);
		return;
	}

	// Get an available OPL2 channel and setup instrument parameters.
	byte i = getFreeChannel(channel);
	if (channel != MIDI_DRUM_CHANNEL) {
		opl2.setInstrument(i, midiInstruments[programMap[channel]]);
	} else {
		if (note >= DRUM_NOTE_BASE && note < DRUM_NOTE_BASE + NUM_MIDI_DRUMS) {
			opl2.setInstrument(i, midiDrums[note - DRUM_NOTE_BASE]);
		} else {
			return;
		}
	}

	// Register channel mapping.
	channelMap[i].midiChannel  = channel;
	channelMap[i].midiNote     = note;
	channelMap[i].midiVelocity = round(velocity / 127.0);
	channelMap[i].op1Level     = round((63 - opl2.getVolume(i, OPERATOR1)) / 63.0);
	channelMap[i].op2Level     = round((63 - opl2.getVolume(i, OPERATOR2)) / 63.0);

	// Set operator output levels based on note velocity.
	setOpl2ChannelVolume(i, channel);

	// Calculate octave and note number and play note!
	byte opl2Octave = 4;
	byte opl2Note = NOTE_C;
	if (channel != MIDI_DRUM_CHANNEL) {
		note = max(24, min(note, 119));
		opl2Octave = 1 + (note - 24) / 12;
		opl2Note   = note % 12;
	}
	opl2.playNote(i, opl2Octave, opl2Note);
}


/**
 * Handle a note off MIDI event to stop playing a note.
 */
void onNoteOff(byte channel, byte note, byte velocity) {
	channel = channel % 16;
	for (byte i = 0; i < OPL2_NUM_CHANNELS; i ++) {
		if (channelMap[i].midiChannel == channel && channelMap[i].midiNote == note) {
			opl2.setKeyOn(i, false);
			break;
		}
	}
}


/**
 * Handle instrument change on the given MIDI channel.
 */
void onProgramChange(byte channel, byte program) {
	programMap[channel % 16] = min(program, 127);
}


/**
 * Handle MIDI control changes on the given channel.
 */
void onControlChange(byte channel, byte control, byte value) {
	channel = channel % 16;

	switch (control) {

		// Change volume of a MIDI channel. (Limited to 0.8 to prevent clipping)
		case CONTROL_VOLUME: {
			channelVolumes[channel] = min(value / 127.0, 0.8);
			for (byte i = 0; i < OPL2_NUM_CHANNELS; i ++) {
				if (channelMap[i].midiChannel == channel && opl2.getKeyOn(i)) {
					setOpl2ChannelVolume(i, channel);
				}
			}
			break;
		}

		// Reset all controller values.
		case CONTROL_RESET_ALL:
			for (byte i = 0; i < MIDI_NUM_CHANNELS; i ++) {
				channelVolumes[channel] = 0.8;
			}
		break;

		// Immediately silence all channels.
		// Intentionally cascade into CONTROL_ALL_NOTES_OFF!
		case CONTROL_51:
		case CONTROL_OMNI_MODE_OFF:
		case CONTROL_OMNI_MODE_ON:
		case CONTROL_MONO_MODE_ON:
		case CONTROL_POLY_MODE_ON:
		case CONTROL_ALL_SOUND_OFF:
			for (byte i = 0; i < OPL2_NUM_CHANNELS; i ++) {
				opl2.setRelease(i, OPERATOR1, 0);
				opl2.setRelease(i, OPERATOR2, 0);
			}

		// Silence all MIDI channels.
		case CONTROL_ALL_NOTES_OFF: {
			for (byte i = 0; i < OPL2_NUM_CHANNELS; i ++) {
				if (channelMap[i].midiChannel == channel) {
					onNoteOff(channelMap[i].midiChannel, channelMap[i].midiNote, 0);
				}
			}
			break;
		}

		// Change instrument
		case CONTROL_GENERAL_PURPOSE_1:
			onProgramChange(channelMap[channel].midiChannel, value);
			break;

		// Ignore any other MIDI controls.
		default:
			break;
	}
}


/**
 * Handle full system reset.
 */
void onSystemReset() {
	opl2.init();

	// Silence all channels and set default instrument.
	for (byte i = 0; i < OPL2_NUM_CHANNELS; i ++) {
		opl2.setKeyOn(i, false);
		opl2.setInstrument(i, midiInstruments[0]);
		oldestChannel[i] = i;
	}

	// Reset default MIDI player parameters.
	for (byte i = 0; i < MIDI_NUM_CHANNELS; i ++) {
		programMap[i] = 0;
		channelVolumes[i] = 0.8;
	}
}
